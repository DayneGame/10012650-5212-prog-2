﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealWpfAssignment
{
    // My customer class is inside a Customer.cs file.
    public partial class MainWindow : Window
    {
        static int index = 0;
        // This is my list that holds all the customers.
        static List<Customer> CustomerDB = new List<Customer>();

        public MainWindow()
        {
            InitializeComponent();
            LoadDb();
            DisplayCustomers();
        }

        // These are the methods

        public void LoadDb() // This Loads the hard-coded people into the static CustomerDB List
        {
            CustomerDB.Add(new Customer("Jaarana", "Kereopa", "123-2514"));
            CustomerDB.Add(new Customer("Sue", "Stook", "123-1263"));
            CustomerDB.Add(new Customer("Jamie", "Allom", "123-3658"));
            CustomerDB.Add(new Customer("Brian", "James", "123-9898"));
        }
        public void DisplayCustomers() // This method displays the Customers to the Listbox called outputbox.
        {
            List<string> Data = new List<string>();

            foreach (var x in CustomerDB)
            {
                var cust = x.GetCustomer();
                Data.Add(cust);
            }
            outputBox.ItemsSource = Data;
        }
        public void ClearDisplay() // Clears the listbox display 
        {
            outputBox.ItemsSource = null;
            outputBox.Items.Clear();
        }
        public void ClearBoxes() // This clears the textboxes of firstname, lastname, phone and search.
        {
            firstname.Text = String.Empty;
            lastname.Text = String.Empty;
            phone.Text = String.Empty;
            search.Text = String.Empty;
        }

        // These are the buttons.

        private void listCust_Click(object sender, RoutedEventArgs e) // When this buttton is clicked it deploys two methods. ClearDisplay and Display Customers.
        {
            ClearDisplay();
            DisplayCustomers();
        }

        private void clearList_Click(object sender, RoutedEventArgs e) // When this button is clicked it deploys the ClearDisplay() method which clears the listbox display
        {
            add.IsEnabled = true;
            ClearDisplay();
            search.Text = "";
            search.Focus();
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            var searchInput = search.Text;

            if (string.IsNullOrWhiteSpace(search.Text)) // This if statement checks to see if search.text is empty or whitespace
            {
                MessageBox.Show("You didn't enter anything to Search", "WPF ASSIGNMENT", MessageBoxButton.OK, MessageBoxImage.Error);
                search.Focus();
            }
            else
            {
                var searchDone = false;
                foreach (var x in CustomerDB)
                {
                    if (x.GetCustomer().Contains(searchInput)) // ths checks if the users search is in the database
                    {
                        System.Diagnostics.Debug.WriteLine(x.GetCustomer().Contains(searchInput));
                        List<string> data = new List<string>();
                        data.Add(x.GetCustomer());
                        System.Diagnostics.Debug.WriteLine(string.Join(" ,", data));
                        outputBox.ItemsSource = data;
                        ClearBoxes();
                        searchDone = true;

                    }
                }
                if (searchDone != true)
                {
                    MessageBox.Show("Couldn't find anyone in the database");
                    searchDone = false;
                }
            }
        }

        private void update_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(firstname.Text) || string.IsNullOrWhiteSpace(lastname.Text) || string.IsNullOrWhiteSpace(phone.Text))
            {
                MessageBox.Show("All Textboxes must be filled to continue");
            }
            else
            {

                CustomerDB.Remove(CustomerDB[index]);
                var fname = firstname.Text;
                var lname = lastname.Text;
                var ph = phone.Text;

                CustomerDB.Insert(index, new Customer(fname, lname, ph));
                
                ClearBoxes();
                ClearDisplay();
                DisplayCustomers();

                MessageBox.Show("User has been added!");
                add.IsEnabled = true;
            }
        }

        private void add_Click(object sender, RoutedEventArgs e) // When this button is clicked it adds what you have put in you firstname, lastname, and phone text box whihc than gets added to the CustomerDB list
        {
            add.IsEnabled = true;
            if (string.IsNullOrWhiteSpace(firstname.Text) || string.IsNullOrWhiteSpace(lastname.Text) || string.IsNullOrWhiteSpace(phone.Text))
            {
                MessageBox.Show("All Textboxes must be filled to continue");
            }
            else
            {
                var fname = firstname.Text;
                var lname = lastname.Text;
                var ph = phone.Text;

                CustomerDB.Add(new Customer(fname, lname, ph));
                ClearBoxes();
                ClearDisplay();
                DisplayCustomers();

                MessageBox.Show("User has been added!");
            }
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            if (outputBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select an Item first!");
                ClearDisplay();
                DisplayCustomers();
            }
            else
            {
                var check = MessageBox.Show("Are you Sure you want to Delete this User?", "WPF ASSIGNMENT", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (check == MessageBoxResult.Yes)
                {
                    {
                        CustomerDB.RemoveAt(index);
                    }
                    ClearBoxes();
                    ClearDisplay();
                    DisplayCustomers();
                    MessageBox.Show("User has been Removed!");
                }
            }
        }

        private void clear_Click(object sender, RoutedEventArgs e) // When this button is clicked it deploys the ClearBoxes() method
        {
            ClearBoxes();
        }

        private void SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            add.IsEnabled = false; // This disables add button
            if (outputBox.SelectedValue != null)
            {
                index = 0;
                //string[] CustDataArray = outputBox.SelectedValue.Split('\t');
                var SelectedCustData = outputBox.SelectedValue as string;
                var CustDataArray = SelectedCustData.Split('\t');

                // FYI ' ' = char / " " = string
                System.Diagnostics.Debug.WriteLine(string.Join(" ,", CustDataArray));

                firstname.Text = CustDataArray[0]; // Outputs FName to firstname.text
                lastname.Text = CustDataArray[1]; // Outputs LName to lastname.text
                phone.Text = CustDataArray[2]; // Outputs Phone to phone.Text
                int i = CustomerDB.FindIndex(a => a.FName == firstname.Text);

                index += i;
            }
        }
        
    }
}
